#Imagen base
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copia de archivos
ADD . /app

#Dependencias sólo las que están en el apartado "Dependencies"
RUN npm install

#Puerto que exponemos
EXPOSE 3002

#Comando que requiere mi app para ejecutarse
CMD ["npm", "start"]
