//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

var path = require('path');
  app.listen(port);

//*-------------------------------------------------------------------------------------
//* Configuramos la parte de la Seguridad para poder enviar peticiones a la BD (MongoDB)
var bodyParser = require( 'body-parser' );
app.use( bodyParser.json() );
app.use( function(req, res, next){
  res.header( "Access-Control-Allow-Origin", "*" );
  res.header( "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept" );
  next();
});

//*--------------------------------------------------------------------------
//* Para trabajar con la base de datos MongoDB
//*--------------------------------------------------------------------------
// Agregamos librería request-json para atacar MLab (MongoDB)
var requestjson = require( "request-json" );
var urlDBMovimientos = "https://api.mlab.com/api/1/databases/bdbanca3m911320/collections/movimientos?apiKey=rfKYRuE0XSDDRCdTQSLQx3ILk6rsbEki";
var clienteMLab = requestjson.createClient( urlDBMovimientos );
//*--------------------------------------------------------------------------

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function( req, res ){

  //* Respondemos con un texto
  res.send('Hola mundo desde App Backend node');

  //* Respondemos con un fichero HTML
  //res.sendFile( path.join(__dirname, 'index.html' ) );

  //* Respondemos con un json
  //res.json( "{ [  {id:1, name:'nombre1'},    {id:2, nombre:'nombre2'},     {id:3, nombre:'nombre3'}   ] }" );

});

//*
//* idcliente es el nombre de la variable donde voy a cachar el ID del cliente en la llamada GET
app.get( '/clientes/:idcliente', function(req, res){
    res.send('Aquí tiene el cliente número ' + req.params.idcliente );
});

app.post( '/', function( req, res ){
  res.send( 'Su petición POST ha sido recibida cambiada' );
});

app.put( '/', function( req, res ){
  res.send( 'Su petición PUT ha sido recibida' );
});

app.delete( '/', function( req, res ){
  res.send( 'Su petición DELETE ha sido recibida' );
});

//*----------------------------------------------------------------------------
//* Servicios API Restful para la base de movimientos en MongoDB
//*----------------------------------------------------------------------------
app.get('/movimientos', function(req, res){

  clienteMLab.get( '', function( err, resM, body ){
    if( err ){
      console.log( err );
    }else{
      res.send( body );
    }
  });

});

app.post( '/movimientos', function( req, res ){
  clienteMLab.post( '', req.body, function( err, resM, body ){
    if( err ){
      console.log( err );
    }else{
      res.send( body );
    }
  });
  //res.send( 'Su petición POST ha sido recibida cambiada' );
});
